//
//  User.swift
//  Spotify
//
//  Created by iD Student on 7/7/15.
//  Copyright (c) 2015 iD Tech. All rights reserved.
//

import Foundation

class User {
    var name : String
    var numFollowers : Int
    var numFollowing: Int
    var followers: [User]
    var following: [User]
    
    init(name: String, numFollowers: Int, numFollowing: Int , followers: [User], following: [User]){
        self.name = name
        self.numFollowers = followers.count
        self.numFollowing = following.count
        self.followers = followers
        self.following = following
    }
    
    func follow(user: User){
         self.following.append(user)
    }
    
    func allFollowers() -> String {
        var str = ""
        var ind = 0
        for user in self.followers {
            str = str + user.name
            ind++
            if ind<followers.count {
                str = str + ", "
            }
        }
        return str
    }
    
    func allFollowing() -> String {
        var str = ""
        var ind = 0
        for user in self.following {
            str = str + user.name
            ind++
            if ind<following.count {
                str = str + ", "
            }
        }
        return str
    }
    
}