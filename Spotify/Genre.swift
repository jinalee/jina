//
//  Genre.swift
//  Spotify
//
//  Created by iD Student on 7/8/15.
//  Copyright (c) 2015 iD Tech. All rights reserved.
//

import Foundation

enum Genre {
    case Pop, Rock, Alternative, Country, Classical
    
    var description : String {
        switch self{
        case .Pop: return "Pop"
        case .Rock: return "Rock"
        case .Alternative: return "Alternative"
        case .Country: return "Country"
        case .Classical: return "Classical"
            
        
        }
    }
}