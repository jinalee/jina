//
//  Time.swift
//  Spotify
//
//  Created by iD Student on 7/8/15.
//  Copyright (c) 2015 iD Tech. All rights reserved.
//

import Foundation

class Time {
    var hours : Int
    var min : Int
    var seconds : Int
    
    init(hours: Int, min:Int, seconds: Int){
        self.hours = hours
        self.min = min
        self.seconds = seconds
    }
    
    func printTime()->String{
        return "\(self.hours):\(self.min):\(self.seconds)"
    }
    
}