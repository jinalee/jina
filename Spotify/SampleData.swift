//
//  SampleData.swift
//  Spotify
//
//  Created by iD Student on 7/8/15.
//  Copyright (c) 2015 iD Tech. All rights reserved.
//

import Foundation

let s1 = Song(title: "Happy", duration: Time(hours:0, min: 4, seconds: 10), artist: "Marina and the Diamonds", genre: Genre.Alternative)
let s2 = Song(title: "Eine Kleine Nachtmusik", duration: Time(hours: 0, min: 3, seconds: 45),artist: "Wolfgang Amadeus Mozart", genre: Genre.Classical)
let s3 = Song(title: "Uptown Funk", duration: Time(hours: 0, min: 4, seconds: 15), artist: "Mark Ronson", genre: Genre.Pop)

let play1 : [Song] = [s1, s2]
let play2 : [Song] = [s2, s3]
let play3 : [Song] = [s3, s1]

let playlist1 = Playlist(list: play1, name: "Playlist 1")
let playlist2 = Playlist(list: play2, name: "Playlist 2")
let playlist3 = Playlist(list: play3, name: "Playlist 3")

let playArray : [Playlist] = [playlist1, playlist2, playlist3]

let u = [User]()
let u1 = User(name: "Jack", numFollowers: 0, numFollowing:0, followers : u, following : u)
let u2 = User(name : "Jill", numFollowers: 0, numFollowing:0, followers : u, following : u)

let me = User(name: "Jina Lee", numFollowers: 2, numFollowing :3, followers:[u1, u2], following: [u1,u2])