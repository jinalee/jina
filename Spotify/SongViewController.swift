//
//  SongViewController.swift
//  Spotify
//
//  Created by iD Student on 7/9/15.
//  Copyright (c) 2015 iD Tech. All rights reserved.
//

import UIKit

class SongViewController: UITableViewController {
    let music = play1
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return music.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("SongCell", forIndexPath: indexPath) as! SongCell
        
        let song = music[indexPath.row]
        cell.SongName.text = song.title
        cell.Artist.text = song.artist
        cell.Time.text = song.duration.printTime()
        cell.Genre.text = song.genre.description
        return cell
    }
    
    

}