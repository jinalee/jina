//
//  PlaylistCell.swift
//  Spotify
//
//  Created by iD Student on 7/8/15.
//  Copyright (c) 2015 iD Tech. All rights reserved.
//

import UIKit

class PlaylistCell: UITableViewCell {
   
    @IBOutlet weak var PlaylistName: UILabel!
    
    @IBOutlet weak var Duration: UILabel!
    
}
