//
//  Song.swift
//  Spotify
//
//  Created by iD Student on 7/7/15.
//  Copyright (c) 2015 iD Tech. All rights reserved.
//

import Foundation

class Song {
    
    var title : String
    var duration : Time
    var artist : String
    var genre: Genre
    
    init(title: String, duration: Time, artist: String, genre: Genre){
        self.title = title
        self.duration = duration
        self.artist = artist
        self.genre = genre
    }
    
    func play(){
        
    }
    
    func pause(){
        
    }
}