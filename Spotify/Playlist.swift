//
//  Playlist.swift
//  Spotify
//
//  Created by iD Student on 7/7/15.
//  Copyright (c) 2015 iD Tech. All rights reserved.
//

import Foundation

class Playlist {
    var list : [Song]
    var name : String
    init(list:[Song], name: String){
        self.list = list
        self.name = name
    }
    
    func addSong(song:Song){
        self.list.append(song)
    }
    
    func removeSong(song: Song){
        list = list.filter() {$0 !== song}
    }
    
    func totalDuration()->String{
        var h : Int = 0
        var m : Int = 0
        var s : Int = 0
        for t in self.list {
            h+=t.duration.hours
            m+=t.duration.min
            s+=t.duration.seconds
        }
        var total = Time(hours: h, min: m, seconds: s)
        return total.printTime()
    }
    
    func searchByGenre(g: Genre) -> [Song] {
        var group = [Song]()
        for song in self.list{
            if song.genre == g {
                group.append(song)
            }
        }
        return group
    }
    
    
    
}