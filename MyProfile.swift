//
//  MyProfile.swift
//  Spotify
//
//  Created by iD Student on 7/8/15.
//  Copyright (c) 2015 iD Tech. All rights reserved.
//

import UIKit

class MyProfile: UIViewController {
   
    @IBOutlet weak var PersonName: UILabel!
    @IBOutlet weak var NumFollowers: UILabel!
    @IBOutlet weak var FollowerNames: UILabel!
    @IBOutlet weak var NumFollowing: UILabel!
    @IBOutlet weak var FollowingNames: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PersonName.text = me.name
        NumFollowers.text = String(me.numFollowers)
        NumFollowing.text = String(me.numFollowing)
        FollowerNames.text = me.allFollowers()
        FollowingNames.text = me.allFollowing()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
