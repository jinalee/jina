//
//  SongCell.swift
//  Spotify
//
//  Created by iD Student on 7/9/15.
//  Copyright (c) 2015 iD Tech. All rights reserved.
//

import UIKit

class SongCell : UITableViewCell{
    
    @IBOutlet weak var SongName: UILabel!
    @IBOutlet weak var Artist: UILabel!
    @IBOutlet weak var Time: UILabel!
    @IBOutlet weak var Genre: UILabel!



}
